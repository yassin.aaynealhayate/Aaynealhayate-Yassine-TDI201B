create  database  AssociationDB
use  AssociationDB

create table Ville(
	Id_ville int primary key ,
	Nom_Ville varchar(30) ,	
	Pays varchar(30)
)


insert into Ville values(2,'Marrakech','Maroc')
insert into Ville values(3,'Rabat','Maroc')
insert into Ville values(4,'Paris','France')

create table Volontaire(
	Id_Vlt int primary key,
	Nom_Vlt  varchar(30) ,
	Prenom_Vlt  varchar(30),
	Mail  varchar(50),
	Mot_Passe  varchar(30),
	Id_Ville int,
	Actif varchar(20),
	foreign key (Id_Ville) references Ville
)

insert into  Volontaire values(11,'Alami','Said','said99@gmail.com','EEE444',2,'Active')
insert into  Volontaire values(2,'Motafi','Ali','ali-motafi@gmail.com','EEE111',3,'Active')


create table Association (
	Id_Ass int primary key ,Nom_Ass varchar(30),
	RaisonSocial varchar(30),
	Adresse varchar(60),
	Telephone varchar(30),Id_Ville int, 
	foreign key (Id_Ville) references Ville
)
 
insert into   Association  values(1,'Association1','RaisonSocial1','Adresse1','0665656565',2)

create table Stage (
	Id_Stage  int primary key,
	Date_Debut date,
	Date_Fin date ,Id_Ass  int ,
	foreign key (Id_Ass) references Association 
	)

insert into   Stage  values(1,'12/05/2020','12/09/2020',1)


create table Demande_Inscription (
	Id_Inscription int primary key identity ,
	Date_Demande date ,
	Id_Vlt int,
	Id_Stage int,
	Etat varchar(30),
	foreign key (Id_Vlt) references Volontaire ,
	foreign key (Id_Stage) references Stage,
	CONSTRAINT chk_Etat CHECK (Etat IN ('En attente', 'Valide', 'refus�', 'accept�'))
	)
 


insert into   Demande_Inscription  values('12/05/2020',2,1,'refus�')